# Scheddy Kong Racing

It's the [Diddy Kong Racing decompilation](https://github.com/DavidSM64/Diddy-Kong-Racing), but with a redesigned scheduler that's made for performance.

* In Scheddy Kong Racing, the main thread tries to keep two graphics tasks at the scheduler. New tasks are created in response to "task done" messages rather than on retrace events.
* When a graphics task is completed by the RDP, the scheduler itself configures the VI to swap to the new buffer rather than have the main thread do the swap when it becomes free. This allows the VI swap to happen sooner.
* Scheddy Kong Racing uses three framebuffers. This allows the RDP to start the next task immediately rather than have to wait for the VI to complete the swap for the previous. Buffer 1 is on-screen, buffer 2 is just completed and buffer 3 can be used for the next task.
* When the main thread and RDP are generating and executing tasks faster than 60 per second, the scheduler queues the completed framebuffers and stops sending tasks to the RDP until a framebuffer becomes free (typically at the next retrace).
* The frame rate is now completely fluid (up to 60 FPS) rather than snapping to 30 FPS, 20 FPS, 15 FPS and so on.
* In the vanilla game, the main and audio threads submit tasks to the scheduler using its command message queue. In Scheddy Kong Racing, the main and audio threads temporarily elevate their thread priority above the scheduler, then either execute the task directly or queue it to be handled by the scheduler when ready. This method of task submission saves some unnecessary thread context switching.
* Audio scheduling is the same as the vanilla game. On every second retrace, the scheduler executes any pending audio task and asks the audio thread for another. Graphics tasks are yielded to allow the audio task to take priority.

For build instructions, see the [Diddy Kong Racing decompilation](https://github.com/DavidSM64/Diddy-Kong-Racing) repository.
